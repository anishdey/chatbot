import ChatBotWidget from './widget/chatBotWidget/chatBotWidget';
import './App.css';

function App() {
  return (
    <div className="App">
      <ChatBotWidget />
    </div>
  );
}

export default App;
