import './chatBotWidget.css';
import React from 'react';

const ChatBotWidget = (props) => {
    const [chatBotToggle, setChatBotToggle] = React.useState(false);
    return (
        <div className={`chatBotWrapper ${!chatBotToggle ? 'chatBotClosed' : ''}`}>
            <div className={`chatBotHeader ${!chatBotToggle ? 'inactiveHeader' : ''}`} onClick={() => setChatBotToggle(!chatBotToggle)}>

            </div>
            <div className={`chatBotBody ${!chatBotToggle ? 'hide' : ''}`}>

            </div>
            <div className={`chatBotFooter ${!chatBotToggle ? 'hide' : ''}`}>

            </div>
        </div>
    )
}
export default ChatBotWidget;
